(function ($, window, document) {

  'use strict';

  $(function () {

    // Video
    $('#main-video').attr('muted', true);

    var videoPlayer = videojs('main-video');
    var productListItems = $('.rel-products__list__item');
    var productListSlider = $('.rel-products__list__slider');
    var checkTime;
    var currentNode = -1;

    function renderPop(el, index) {
      var text = '<section class="pop__content">' +
                 '<div class="pop__img"><a href="' + el.url + '" target="_blank"><img class="img-fluid" src="' + el.imageUri + '" /></a></div>' +
                 '<div class="pop__text row no-gutters">' +
                   '<div class="pop__text__productname col-xs-10"><h2 class="pop__text__productname__name">' + el.content + '</h2><div class="pop__text__productname__brand text-muted">' + el.brand + '</div></div>' +
                   '<span class="pop__text__price pull-right col-xs-2">' + el.price + '</span>' +
                   // '<div class="pop--text--duration">Duration from ' + el.start + 's to ' + el.end + 's' + '</div>' +
                 '</div>' +
                 '<div class="pop__buylink"><a href="' + el.url + '" target="_blank" class="btn btn-primary">ADD TO BAG</a></div>' +
                 '</section>';
      document.getElementById('pop').innerHTML = text;
      currentNode = index;
      setActiveSlideItem(index);
      $('.slickslider').slick('slickGoTo', index);
      // console.log(index);
    }

    function checkPop(time) {
      nodes.forEach(function (pop, key) {
        if (time >= pop.start && time <= pop.end) {
          if (currentNode !== key) {
            renderPop(pop, key);
            currentNode = key;
            // productListSlider.addClass('hidden-xs-up');
          }
        }
        else if (currentNode === key && time >= pop.end) {
          document.getElementById('pop').innerHTML = '';
          // productListSlider.removeClass('hidden-xs-up');
          unsetActiveSlideItem();
        }
      });
    }

    function setActiveSlideItem(index) {
      productListItems.eq(index).addClass('active');
      $('.rel-products__detail').addClass('open');
      $('.rel-products__detail').removeClass('close');
    }

    function unsetActiveSlideItem() {
      productListSlider.find('.active').removeClass('active');
      $('.rel-products__detail').addClass('close');
      $('.rel-products__detail').removeClass('open');
    }

    function renderSeconds(time) {
      document.getElementById('seconds').innerHTML = time.toFixed(2) + 's';
    }

    videoPlayer.ready(function () {
      // videoPlayer.setTimeout(videoPlayer.currentTime(155).play(), 1000);
      // videoPlayer.muted(true);
      // videoPlayer.currentTime(155).play();
      // videoPlayer.setTimeout(unmute, 3000);
      videoPlayer.play();
    });

    videoPlayer.on('loadedmetadata', function () {
      // console.log('loaded metadata');
    });

    videoPlayer.on('seeking', function () {
      timeElapsed();
      // console.log('seeking event');
    });

    videoPlayer.on('pause', function () {
      videoPlayer.clearInterval(checkTime);
      // clearInterval(checkTime);
      // console.log('pause event');
    });

    videoPlayer.on('play', function () {
      checkTime = videoPlayer.setInterval(timeElapsed, 200);
      // console.log('play event');
    });

    videoPlayer.on('ended', function () {
      videoPlayer.clearInterval(checkTime);
      // console.log('ended event');
    });

    // function unmute() {
    //   videoPlayer.volume(0.5);
    //   $('.vjs-volume-menu-button').click();
    //   // videoPlayer.muted(false);
    // }

    function timeElapsed() {
      // var videoTime = Math.round(player.getCurrentTime());
      // videoPlayer.currentTime(120).play();
      var videoTime = videoPlayer.currentTime();
      checkPop(videoTime);
      renderSeconds(videoTime);
      // console.log(videoTime);
    }

    var nodes = [
      {
        brand: 'Nautica',
        content: 'STRIPED DOUBLE LAYER TANK',
        price: '$23',
        start: 4.31,
        end: 9.8,
        url: 'http://www.nautica.com/striped-double-layer-tank/71K200.html?dwvar_71K200_color=101#uuid=5e9e3cf45f6e13e5f3bebf5335&display=70&sz=99',
        imageUri: '/assets/img/products/Item 6.jpg'
      },
      {
        brand: 'Nautica',
        content: 'STRIPED WIDE LEG CREPE PANT',
        price: '$23',
        start: 10.40,
        end: 12.83,
        url: 'http://www.nautica.com/striped-wide-leg-crepe-pant/71P102.html?dwvar_71P102_color=482#uuid=0357bf3a760863bd411f745c90',
        imageUri: '/assets/img/products/Item 7.jpg'
      },
      {
        brand: 'Nautica',
        content: 'LINEN-BLEND PIECED STRIPE DRESS',
        price: '$23',
        start: 12.83,
        end: 14.5,
        url: 'http://www.nautica.com/linen-blend-pieced-stripe-dress/71D216.html?dwvar_71D216_color=101#uuid=1448985afcafd975953931b1d9',
        imageUri: '/assets/img/products/Item 8.jpg'
      },
      {
        brand: 'Nautica',
        content: 'CHAMBRAY TRENCH COAT',
        price: '$23',
        start: 14.77,
        end: 20.29,
        url: 'http://www.nautica.com/chambray-trench-coat/7100TR.html?dwvar_7100TR_color=481#uuid=4cef54386d4c898d7fbf226f93',
        imageUri: '/assets/img/products/Item 9.jpg'
      },
      {
        brand: 'Nautica',
        content: 'QUICK-DRY ANCHOR PRINT SWIM TRUNK',
        price: '$23',
        start: 23.2,
        end: 24.56,
        url: 'http://www.nautica.com/quick-dry-anchor-print-swim-trunk/T71635.html?dwvar_T71635_color=401#uuid=cbed914ce3a2052c7c288fd319',
        imageUri: '/assets/img/products/Item 10.jpg'
      }
    ];

    // Slick slider for products list
    $('.slickslider').slick({
      slidesToShow: 3.5,
      swipeToSlide: true,
      // dots: true,
      centerPadding: '15px',
      arrows: false,
      // centerMode: true,
      infinite: false,
      // variableWidth: true,
      focusOnSelect: true,
      // mobileFirst: true,
      responsive: [
        {
          breakpoint: 543,
          settings: {
            slidesToShow: 3.5
          }
        },
        {
          breakpoint: 767,
          settings: {
            slidesToShow: 6.3
          }
        }
      ]
    });
  });


})(jQuery, window, document);
